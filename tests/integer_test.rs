extern crate bencoding;
use bencoding::integer::Integer;
use bencoding::BendcodeElement;

#[test]
fn test_encode() {
    let min_integer_64_bit = format!("i{}e", i64::MIN);
    let max_integer_64_bit = format!("i{}e", i64::MAX);
    let test_cases = [
        (1, "i1e"),
        (-1, "i-1e"),
        (0, "i0e"),
        (-2213, "i-2213e"),
        (2134, "i2134e"),
        (i64::MIN, min_integer_64_bit.as_str()),
        (i64::MAX, max_integer_64_bit.as_str()),
    ];
    for (test_case, expected_value) in test_cases.iter() {
        let value = Integer::encode(test_case);
        assert_eq!(
            value.as_str(),
            *expected_value,
            "this value `{}` suppose to return `{}` but return `{}`",
            test_case,
            expected_value,
            value
        );
    }
}
#[test]
fn test_decode_with_valid_value(){
    let min_integer_64_bit = format!("i{}e", i64::MIN);
    let max_integer_64_bit = format!("i{}e", i64::MAX);
    let test_cases = [
        ("i1e",1),
        ("i-1e",-1),
        ("i0e",0),
        ("i-2213e",-2213),
        ("i2134e",2134),
        (min_integer_64_bit.as_str(),i64::MIN),
        (max_integer_64_bit.as_str(),i64::MAX),
    ];
    for (test_case, expected_value) in test_cases.iter() {
        let value = Integer::decode(&test_case.to_string());
        assert!(
            value.is_ok(),
            "this value {} suppose to be ok",
            test_case
        );
        let value = value.unwrap();
        assert_eq!(
            value,
            *expected_value,
            "this value `{}` suppose to return `{}` but return `{}`",
            test_case,
            expected_value,
            value
        );
    }
}
#[test]
fn test_decode_with_invalid_value() {
    let test_cases = [
        "",
        "aaaa",
        "p3e",
        "iae",
        "i32342ae",
        "i1.234e",
        "21",
        "i1_e",
        "i21_23e",
        "ii21e",
        "i-33ee",
        "ii-22e"
    ];
    for test_case in test_cases.iter(){
        let value = Integer::decode(&test_case.to_string());
        assert!(
            value.is_err(),
            "this value `{}` is suppose to return an error",
            test_case
        );
    }
}
#[test]
fn test_from_decoded_data() {
    let min_integer_64_bit = format!("i{}e", i64::MIN);
    let max_integer_64_bit = format!("i{}e", i64::MAX);
    let test_cases = [
        (1, "i1e"),
        (-1, "i-1e"),
        (0, "i0e"),
        (-2213, "i-2213e"),
        (2134, "i2134e"),
        (i64::MIN, min_integer_64_bit.as_str()),
        (i64::MAX, max_integer_64_bit.as_str()),
    ];
    for (decoded_value, encoded_value) in test_cases.iter() {
        let integer = Integer::from_decoded_data(decoded_value);
        
        assert_eq!(
            integer.get_encoded(),
            *encoded_value,
            "the methode `get_encoded()` suppose to return `{}` but return `{}`",
            encoded_value,
            integer.get_encoded()
        );
        assert_eq!(
            integer.get_decoded(),
            *decoded_value,
            "the methode `get_decoded()` suppose to return `{}` but return `{}`",
            decoded_value,
            integer.get_decoded()
        );
    }
}

#[test]
fn test_from_encoded_data_with_value() {
    let min_integer_64_bit = format!("i{}e", i64::MIN);
    let max_integer_64_bit = format!("i{}e", i64::MAX);
    let test_cases = [
        ("i1e",1),
        ("i-1e",-1),
        ("i0e",0),
        ("i-2213e",-2213),
        ("i2134e",2134),
        (min_integer_64_bit.as_str(),i64::MIN),
        (max_integer_64_bit.as_str(),i64::MAX),
    ];

    for (encoded_value, expected_decoded_value) in test_cases.iter(){
        let integer = Integer::from_encoded_data(&encoded_value.to_string());
        assert!(
            integer.is_ok(),
            "this value `{}` suppose to construct a valid Integer but return error `{}`",
            encoded_value,
            integer.err().unwrap()
        );
        let integer = integer.unwrap();
        assert_eq!(
            integer.get_encoded(),
            encoded_value.to_string(),
            "the methode `get_encoded()` suppose to return `{}` but return `{}`",
            encoded_value,
            integer.get_encoded()
        );

        assert_eq!(
            integer.get_decoded(),
            *expected_decoded_value,
            "the methode `get_decoded()` suppose to return `{}` but return `{}`",
            expected_decoded_value,
            integer.get_decoded()
        );
    }
}

#[test]
fn test_from_encoded_data_with_invalid_value() {
    let test_cases = [
        "",
        "aaaa",
        "p3e",
        "iae",
        "i32342ae",
        "i1.234e",
        "21",
        "i1_e",
        "i21_23e",
        "ii21e",
        "i-33ee",
        "ii-22e"
    ];
    for encoded_data in test_cases.iter() {
        let integer = Integer::from_encoded_data(&encoded_data.to_string());
        assert!(
            integer.is_err(),
            "this value `{}` suppose to construct an invalid Integer",
            encoded_data
        )
    }
}