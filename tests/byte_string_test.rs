extern crate bencoding;
use bencoding::byte_string::ByteString;
use bencoding::BendcodeElement;

#[test]
fn test_encode_with_valid_value() {
    let test_cases = [
        ("1", "1:1"),
        ("a", "1:a"),
        ("abc", "3:abc"),
        ("12asc", "5:12asc"),
        ("!!@3221da", "9:!!@3221da"),
        ("__--dadsed", "10:__--dadsed"),
        ("a:as", "4:a:as"),
        (":", "1::"),
        ("", "0:"),
        (
            "zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz",
            "69:zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz",
        ),
    ];
    for (test_case, expected_value) in test_cases.iter() {
        let value = ByteString::encode(&test_case.to_string());
        assert!(
            value.is_ok(),
            "this value `{}` suppose to be ok",
            test_case
        );
        let value = value.unwrap();
        assert_eq!(
            value,
            expected_value.to_string(),
            "this value `{}` suppose to return `{}` but return `{}`",
            test_case,
            expected_value,
            value
        );
    }
}

#[test]
fn test_encode_with_invalid_value() {
    let test_cases = ["1:1", "1:a", "5:a2cd!", "2:asdasdewqd"];
    for test_case in test_cases.iter() {
        let value = ByteString::encode(&test_case.to_string());
        assert!(
            value.is_err(),
            "this value `{}` suppose to return an error",
            test_case,
        );
    }
}

#[test]
fn test_decode_with_valid_value() {
    let test_cases = [
        ("1:1", "1"),
        ("1:a", "a"),
        ("3:abc", "abc"),
        ("5:12asc", "12asc"),
        ("9:!!@3221da", "!!@3221da"),
        ("10:__--dadsed", "__--dadsed"),
        ("4:a:as", "a:as"),
        ("1::", ":"),
        ("0:", ""),
        (
            "69:zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz",
            "zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz",
        ),
    ];
    for (test_case, expected_value) in test_cases.iter() {
        let value = ByteString::decode(&test_case.to_string());
        assert!(
            value.is_ok(),
            "this value `{}` suppose to be ok",
            test_case
        );
        let value = value.unwrap();
        assert_eq!(
            value,
            expected_value.to_string(),
            "this value `{}` suppose to return `{}` but return `{}`",
            test_case,
            expected_value,
            value
        );
    }
}

#[test]
fn test_decode_with_invalid_value() {
    let test_cases = [
        "asd",
        "1!2e",
        "1:ac",
        "1:",
        "10:!b3d2f1hi",
        ":",
        "::",
        "abc:3",
        "5!abcde",
        "a3:abc"
    ];
    for test_case in test_cases.iter() {
        let value = ByteString::decode(&test_case.to_string());
        assert!(
            value.is_err(),
            "this value `{}` suppose to return an error",
            test_case,
        );
    }
}
// test if the ByteString objet is return, is decoded data is valid 
#[test]
fn test_decoded_data_with_valid_value() {
    let test_cases = [
        ("1", "1:1"),
        ("a", "1:a"),
        ("abc", "3:abc"),
        ("12asc", "5:12asc"),
        ("!!@3221da", "9:!!@3221da"),
        ("__--dadsed", "10:__--dadsed"),
        ("a:as", "4:a:as"),
        (":", "1::"),
        ("", "0:"),
        (
            "zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz",
            "69:zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz",
        ),
    ];
    for (decoded_value,expected_encoded_value) in test_cases.iter() {
        let byte_string = ByteString::from_decoded_data(&decoded_value.to_string());
        assert!(
            byte_string.is_ok(),
            "this value `{}` suppose to construct a valid ByteString but return this error `{}`",
            decoded_value,
            byte_string.err().unwrap()
        );
        let byte_string = byte_string.unwrap();
        assert_eq!(
            byte_string.get_encoded(),
            expected_encoded_value.to_string(),
            "the methode `get_encoded()` suppose to return `{}` but return `{}`",
            expected_encoded_value,
            byte_string.get_encoded()
        );

        assert_eq!(
            byte_string.get_decoded(),
            decoded_value.to_string(),
            "the methode `get_decoded()` suppose to return `{}` but return `{}`",
            decoded_value.to_string(),
            byte_string.get_decoded()
        );
    }
}

#[test]
fn test_from_decoded_data_with_invalid_value() {
    let test_cases = ["1:1", "1:a", "5:a2cd!", "2:asdasdewqd","a3:abc"];
    for encoded_value in test_cases.iter() {
        let byte_string = ByteString::from_decoded_data(&encoded_value.to_string());
        assert!(
            byte_string.is_err(),
            "this value `{}` suppose to construct an invalid ByteString",
            encoded_value
        );
    }
}

#[test]
fn test_from_encoded_data_with_valid_value() {
    let test_cases = [
        ("1:1", "1"),
        ("1:a", "a"),
        ("3:abc", "abc"),
        ("5:12asc", "12asc"),
        ("9:!!@3221da", "!!@3221da"),
        ("10:__--dadsed", "__--dadsed"),
        ("4:a:as", "a:as"),
        ("1::", ":"),
        ("0:", ""),
        (
            "69:zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz",
            "zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz",
        ),
    ];
    for (encoded_value,expected_decoded_value) in test_cases.iter() {
        let byte_string = ByteString::from_encoded_data(&encoded_value.to_string());
        assert!(
            byte_string.is_ok(),
            "this value `{}` suppose to construct a valid ByteString but return this error `{}`",
            encoded_value,
            byte_string.err().unwrap()
        );
        let byte_string = byte_string.unwrap();

        assert_eq!(
            byte_string.get_encoded(),
            encoded_value.to_string(),
            "the methode `get_encoded()` suppose to return `{}` but return `{}`",
            encoded_value,
            byte_string.get_encoded()
        );
        assert_eq!(
            byte_string.get_decoded(),
            expected_decoded_value.to_string(),
            "the methode `get_decoded()` suppose to return `{}` but return `{}`",
            expected_decoded_value.to_string(),
            byte_string.get_decoded()
        );
    }
}