use regex::Regex;
use crate::BendcodeElement;

pub struct ByteString {
    encoded_data: String,
    decoded_data: String,
}

impl super::BendcodeElement for ByteString {
    fn get_encoded(&self) -> String {
        self.encoded_data.to_string()
    }
    fn is_valid(el: &String) -> bool {
        lazy_static! {
            static ref PATERN: Regex = ByteString::get_patern();
        }
        match PATERN.captures(el.as_str()) {
            None=>false,
            Some(caps)=>{
                let len = &caps["len"].parse::<usize>().unwrap();
                let data = &caps["data"];
                *len==data.len() && caps[0].len()==el.len()
            }
        }
    }
    fn get_patern() -> Regex {
        Regex::new(r"(?P<len>\d+):(?P<data>.*)").unwrap()
    }
}

impl ByteString {
    pub fn from_decoded_data(data: &String) -> Result<ByteString, &'static str> {
        match ByteString::encode(data) {
            Ok(v) => Ok(ByteString {
                encoded_data: v,
                decoded_data: data.to_string(),
            }),
            Err(e) => Err(e),
        }
    }
    pub fn from_encoded_data(data:&String)->Result<ByteString, &'static str>{
        match ByteString::decode(data) {
            Ok(v) => Ok(ByteString {
                encoded_data: data.to_string(),
                decoded_data: v,
            }),
            Err(e) => Err(e),
        }
    }
    // encode a string, the string must not follow the format of the encoded form
    pub fn encode(data: &String) -> Result<String, &'static str> {
        lazy_static!{
            static ref PATERN:Regex = ByteString::get_patern();
        }
        if PATERN.is_match(data.as_str()) {
            Err("the data shouldn't be encoded")
        } else {
            Ok(format!("{}:{}", data.len(), data).to_string())
        }
    }
    pub fn decode(data:&String)->Result<String, &'static str> {
        lazy_static!{
            static ref PATERN:Regex = ByteString::get_patern();
        }
        if ByteString::is_valid(data) {
            let caps = PATERN.captures(data.as_str()).unwrap();
            Ok(String::from(&caps["data"]))
        }else{
            Err("data don't respect the encoding")
        }
    }
    pub fn get_decoded(&self)->String{
        self.decoded_data.to_string()
    }
}

