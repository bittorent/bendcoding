extern crate regex;
#[macro_use] extern crate lazy_static;
pub mod byte_string;
pub mod integer;

pub trait BendcodeElement {
    fn get_encoded(&self) -> String;
    fn is_valid(el:&String)->bool;
    fn get_patern()->regex::Regex;
}