use crate::BendcodeElement;
use regex::Regex;

pub struct Integer{
    encoded_data: String,
    decoded_data: i64,
}
impl super::BendcodeElement for Integer {
    fn get_encoded(&self)->String {
        self.encoded_data.to_string()
    }
    fn get_patern()->Regex {
        Regex::new(r"i(?P<data>-?\d*)e").unwrap()
    }
    fn is_valid(el:&String)->bool {
        lazy_static! {
            static ref PATERN: Regex = Integer::get_patern();
        }
        match PATERN.captures(el.as_str()){
            Some(caps)=>{
                caps[0].len()==el.len()
            },
            None=>false
        }
    }
}

impl Integer {
    pub fn decode(data:&String)->Result<i64,&'static str> {
        lazy_static!{
            static ref PATERN: Regex = Integer::get_patern();
        }
        if Integer::is_valid(data) {
            let caps = PATERN.captures(data.as_str()).unwrap();
            let cap = &caps["data"];
            Ok(cap.parse::<i64>().unwrap())
        }else{
            Err("data don't respect the encoding")
        }
    }
    pub fn encode(data:&i64)->String {
        format!("i{}e",*data).to_string()
    }
    pub fn from_decoded_data(data:&i64)->Integer {
        Integer{
            decoded_data:*data,
            encoded_data:Integer::encode(data)
        }
    }
    pub fn from_encoded_data(data:&String)->Result<Integer,&'static str> {
        if !Integer::is_valid(data) {
            Err("data don't respect the encoding")
        }else{
            match Integer::decode(data) {
                Ok(v) => Ok(Integer{
                    decoded_data:v,
                    encoded_data:data.to_string()
                }),
                Err(e)=>Err(e)
            }
        }
    }
    pub fn get_decoded(&self)->i64{
        self.decoded_data
    }
}