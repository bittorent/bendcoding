# Bendcoding
A rust implementation of the Bendcoding from the Bittorent protocol (in developpement)
# Usage
There are two ways to use this library first using the object from the `BendcodingElement` Trait or second, using the static method from the `BendcodingElement` implementation to only work with basic type.

## Using `BendcodingElement`
with the `BencodingElement` you can create an immutable object using the `from_decoded_data` or `from_encoded_data` associate with the implementation of `BendcodingElement`. They are not in the trait due to there signature. By example

```
let byte_string = ByteString::from_decoded_data(String::from("abc"));
byte_string.get_encoded();
```
## Using static method
you don't have to create an object. You can simply decode or encode a type using static method `decode` or `encode`. By example

```
let encoded_data = Integer::encode(-12);
let decoded_data = Integer::decode("i100e")
```

# Reference
http://bittorrent.org/beps/bep_0003.html